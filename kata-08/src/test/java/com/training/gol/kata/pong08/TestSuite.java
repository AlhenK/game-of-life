package com.training.gol.kata.pong08;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestSuite {
    private static final int ALIVE = 1;
    private static final int DEAD = 0;
    private static final int X = 1;
    private static final int Y = 1;

    /*
      Task:
      Add parametrised tests for multiple test cases of the matrix state
   */

    @Test
    public void givenMatrixCellX1Y1ReturnsStateAlive() {
        int[][] matrix = {
                {0, 0, 0},
                {0, 1, 0},
                {0, 0, 0}
        };
        Board08 board = new Board08(matrix);
        Assertions.assertEquals(ALIVE, board.checkCell(X, Y));
    }

    @Test
    public void givenUnderpopulatedCellX1Y1NextStateDead() {
        int[][] matrix = {
                {0, 0, 0},
                {0, 1, 0},
                {0, 0, 0}
        };
        Board08 board = new Board08(matrix);
        board.step();
        Assertions.assertEquals(DEAD, board.checkCell(X, Y));
    }

    @Test
    public void givenOtherUnderpopulatedCellX1Y1NextStateDead() {
        int[][] matrix = {
                {0, 0, 0},
                {0, 1, 0},
                {0, 0, 1}
        };
        Board08 board = new Board08(matrix);
        board.step();
        Assertions.assertEquals(DEAD, board.checkCell(X, Y));
    }

    @Test
    public void givenOvercrowdingNeighboursCellX1Y1NextStateDead() {
        int[][] matrix = {
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}
        };
        Board08 board = new Board08(matrix);
        board.step();
        Assertions.assertEquals(DEAD, board.checkCell(X, Y));
    }

    @Test
    public void givenSurvivalNeighboursCellX1Y1NextStateAlive() {
        int[][] matrix = {
                {0, 0, 0},
                {0, 1, 1},
                {0, 0, 1}
        };
        Board08 board = new Board08(matrix);
        board.step();
        Assertions.assertEquals(ALIVE, board.checkCell(X, Y));
    }

    @Test
    public void givenReproductiveNeighboursCellX1Y1NextStateAlive() {
        int[][] matrix = {
                {0, 0, 1},
                {0, 0, 1},
                {0, 0, 1}
        };
        Board08 board = new Board08(matrix);
        board.step();
        Assertions.assertEquals(ALIVE, board.checkCell(X, Y));
    }
}
