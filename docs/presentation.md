### Objectives
- Software technical practices
- Explain Coderetreat event
- TDD example with Game of Life
- Pair programming and ping-pong
- Testing multiple input data

#Global Day of Coderetreat
####A community event to practice professional software development
![Game of Life board example](img/coderetreat.png)
### <https://www.coderetreat.org/> 

###Learning Goals
- Have fun!
- Use dedicated practice to explore new techniques
- Write better code, using Test-Driven Development, pairing, and other software technical practices
- Take risks and experiment

#Conway's GAME OF LIFE

![Game of Life board example](img/board-example.jpg)

##Rules 

![Game of Life rules](img/game-of-life-rules.png)

# How to start
- Test first
- Don't test setters & getters
- Decompose the task and take a small part

### It's a Board Game with Matrix

![Board game with matrix](img/matrix.png)

### Entities we might make up

![Application entities](img/entities.png)

## Take a small part
![Application entities](img/check-cell.png)

## Let's check the state of the cell
![Application entities](img/object-model.png)


## References:

- [Test Driven Development: By Example, Kent Beck](https://www.amazon.com/Test-Driven-Development-Kent-Beck/dp/0321146530)
- ![Test Driven Development cover](img/TDD-by-example-Kent-Beck.jpg)
    

- [Test-Driven Java Development, Viktor Farcic, Alex Garcia ](https://www.amazon.com/Test-Driven-Java-Development-Viktor-Farcic-ebook/dp/B00YSIM3SC)
- ![Test Driven Development cover](img/TDD_ViktorFarcic_AlexGarcia.jpg)