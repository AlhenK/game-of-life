package com.training.gol;

public class Board {
    private static final int ALIVE = 1;
    private static final int DEAD = 0;
    private static final int X = 1;
    private static final int Y = 1;
    private final int[][] matrix;

    public Board(int[][] matrix) {
        this.matrix = matrix;
    }

    public int checkCell(int x, int y) {
        return matrix[x][y];
    }

    public void step() {
        int aliveNeighboursCount = 0;
        for (int j = 0; j < matrix.length; j++) {
            for (int i = 0; i < matrix.length; i++) {
                if (!(i == X && j == Y)) {
                    aliveNeighboursCount += matrix[i][j];
                }
            }
        }

        if (aliveNeighboursCount < 2) {
            matrix[1][1] = DEAD;
        } else if (aliveNeighboursCount > 3) {
            matrix[1][1] = DEAD;
        } else if (aliveNeighboursCount == 3) {
            matrix[1][1] = ALIVE;
        }
    }
}
