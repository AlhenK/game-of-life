package com.training.gol;

import java.util.stream.Stream;

public class UnderPopulatedNeighboursAroundAliveCell {

    public static Stream<Object[]> stream = Stream.of(
            new Object[]{new int[][]{
                    {0, 0, 0},
                    {0, 1, 0},
                    {0, 0, 0}}},
            new Object[]{new int[][]{
                    {1, 0, 0},
                    {0, 1, 0},
                    {0, 0, 0}}},
            new Object[]{new int[][]{
                    {0, 1, 0},
                    {0, 1, 0},
                    {0, 0, 0}}},
            new Object[]{new int[][]{
                    {0, 0, 1},
                    {0, 1, 0},
                    {0, 0, 0}}},
            new Object[]{new int[][]{
                    {0, 0, 0},
                    {0, 1, 1},
                    {0, 0, 0}}},
            new Object[]{new int[][]{
                    {0, 0, 0},
                    {0, 1, 0},
                    {0, 0, 1}}},
            new Object[]{new int[][]{
                    {0, 0, 0},
                    {0, 1, 0},
                    {0, 1, 0}}},
            new Object[]{new int[][]{
                    {0, 0, 0},
                    {0, 1, 0},
                    {1, 0, 0}}},
            new Object[]{new int[][]{
                    {0, 0, 0},
                    {1, 1, 0},
                    {0, 0, 0}}}
    );
}
