package com.training.gol;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class TestSuite {
    private static final int ALIVE = 1;
    private static final int DEAD = 0;
    private static final int TEST_CELL_X = 1;
    private static final int TEST_CELL_Y = 1;

    @Test
    public void givenMatrixCellX1Y1ReturnsStateAlive() {
        int[][] matrix = {
                {0, 0, 0},
                {0, 1, 0},
                {0, 0, 0}
        };
        Board board = new Board(matrix);
        Assertions.assertEquals(ALIVE, board.checkCell(TEST_CELL_X, TEST_CELL_Y));
    }

    @ParameterizedTest
    @MethodSource("underPopulatedNeighboursAroundAliveCell")
    public void givenUnderpopulatedNeighboursCellX1Y1NextStateDead(int[][] matrix) {
        Board board = new Board(matrix);
        board.step();
        Assertions.assertEquals(DEAD, board.checkCell(TEST_CELL_X, TEST_CELL_Y));
    }

    @ParameterizedTest
    @MethodSource("survivalNeighboursAroundAliveCell")
    public void givenSurvivalNeighboursCellX1Y1NextStateAlive(int[][] matrix) {
        Board board = new Board(matrix);
        board.step();
        Assertions.assertEquals(ALIVE, board.checkCell(TEST_CELL_X, TEST_CELL_Y));
    }

    @ParameterizedTest
    @MethodSource("OverCrowdingNeighboursAroundAliveCell")
    public void givenOvercrowdingNeighboursCellX1Y1NextStateDead(int[][] matrix) {
        Board board = new Board(matrix);
        board.step();
        Assertions.assertEquals(DEAD, board.checkCell(TEST_CELL_X, TEST_CELL_Y));
    }

    @ParameterizedTest
    @MethodSource("ReproductiveNeighboursAroundDeadCell")
    public void givenReproductiveNeighboursCellX1Y1NextStateAlive(int[][] matrix) {
        Board board = new Board(matrix);
        board.step();
        Assertions.assertEquals(ALIVE, board.checkCell(TEST_CELL_X, TEST_CELL_Y));
    }

    private static Stream<Object[]> underPopulatedNeighboursAroundAliveCell() {
        return UnderPopulatedNeighboursAroundAliveCell.stream;
    }

    private static Stream<Object[]> survivalNeighboursAroundAliveCell() {
        return SurvivalNeighboursAroundAliveCell.stream;
    }

    private static Stream<Object[]> OverCrowdingNeighboursAroundAliveCell() {
        return OverCrowdingNeighboursAroundAliveCell.stream;
    }

    private static Stream<Object[]> ReproductiveNeighboursAroundDeadCell() {
        return ReproductiveNeighboursAroundDeadCell.stream;
    }
}

