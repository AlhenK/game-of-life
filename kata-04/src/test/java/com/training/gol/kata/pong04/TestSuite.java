package com.training.gol.kata.pong04;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestSuite {
    private static final int ALIVE = 1;
    private static final int X = 1;
    private static final int Y = 1;

     /*
      Task:
      Create a test for Overcrowding rule
      if it's happened to be green then add a test for Survival rule
      test should be red but build - ok
   */

    @Test
    public void givenMatrixCellX1Y1ReturnsStateAlive() {
        int[][] matrix = {
                {0, 0, 0},
                {0, 1, 0},
                {0, 0, 0}
        };
        Board04 board = new Board04(matrix);
        Assertions.assertEquals(ALIVE, board.checkCell(X, Y));
    }

    @Test
    public void givenUnderpopulatedCellX1Y1NextStateDead() {
        int[][] matrix = {
                {0, 0, 0},
                {0, 1, 0},
                {0, 0, 0}
        };
        Board04 board = new Board04(matrix);
        board.step();
        Assertions.assertEquals(0, board.checkCell(X, Y));
    }
}
