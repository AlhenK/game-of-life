package com.training.gol.kata.pong04;

public class Board04 {
    private final int[][] matrix;

    public Board04(int[][] matrix) {
        this.matrix = matrix;
    }

    public int checkCell(int x, int y) {
        return matrix[x][y];
    }

    public void step() {
        int aliveNeighbors = 0;
        for (int j = 0; j < matrix.length; j++) {
            for (int i = 0; i < matrix.length; i++) {
                if ((i == 1 && j == 1)) {
                    aliveNeighbors += matrix[i][j];
                }
            }
        }
        if (aliveNeighbors < 2) {
            matrix[1][1] = 0;
        }
    }
}
