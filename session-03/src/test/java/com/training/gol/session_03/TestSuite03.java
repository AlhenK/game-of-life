package com.training.gol.session_03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestSuite03 {
    Board board;

    @Test
    public void givenInitBoardReturnsBoardSize3x3(){
        int[][] matrix = new int[][]{
                {0, 0, 0},
                {0, 1, 0},
                {0, 0, 0}
        };
        board = new Board(matrix);
        Assertions.assertEquals(board.cells.size(), matrix[0].length * matrix.length);
    }

    @Test
    public void givenSetBoardReturnsCellX1Y1StateAlive(){
        int x = 1;
        int y = 1;
        int[][] matrix = new int[][]{
                {0, 0, 0},
                {0, 1, 0},
                {0, 0, 0}
        };
        board = new Board(matrix);

        int state = board.getCellState(x, y);
        Assertions.assertEquals(state, 1);
    }

    @Test
    public void givenUnderpopulationTheCellNextStateIsDead(){
        int x = 3;
        int y = 3;
        int[][] matrix = new int[][]{
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0}
        };
        board = new Board(matrix);

        board.print();
        board.step();
        board.print();

        int state = board.getCell(x, y).getState();
        Assertions.assertEquals(state, 0);
    }

    @Test
    public void givenTwoNeighboursTheCellNextStateIsAlive(){
        int x = 3;
        int y = 3;
        int[][] matrix = new int[][]{
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 1, 0, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0}
        };
        board = new Board(matrix);

        board.print();
        board.step();
        board.print();

        int state = board.getCell(x, y).getState();
        Assertions.assertEquals(state, 1);
    }

    @Test
    public void givenThreeNeighboursTheCellNextStateIsAlive(){
        int x = 3;
        int y = 3;
        int[][] matrix = new int[][]{
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 1, 0, 0, 0},
                {0, 0, 0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0}
        };
        board = new Board(matrix);

        board.print();
        board.step();
        board.print();

        int state = board.getCell(x, y).getState();
        Assertions.assertEquals(state, 1);
    }

    @Test
    public void givenOvercrowdingTheCellNextStateIsDead(){
        int x = 3;
        int y = 3;
        int[][] matrix = new int[][]{
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0}
        };
        board = new Board(matrix);

        board.print();
        board.step();
        board.print();

        int state = board.getCell(x, y).getState();
        Assertions.assertEquals(state, 0);
    }

    @Test
    public void givenThreeAliveCellsReproduceNewAlive(){
        int x = 3;
        int y = 3;
        int[][] matrix = new int[][]{
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 0},
                {0, 0, 0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0}
        };
        board = new Board(matrix);
        int state = board.getCell(x, y).getState();
        Assertions.assertEquals(state, 0);

        board.print();
        board.step();
        board.print();

        state = board.getCell(x, y).getState();
        Assertions.assertEquals(state, 1);
    }

    @Test
    public void givenTwoAliveNeighboursFindNeighboursReturnsTwo(){
        int[][] matrix = new int[][]{
                {0, 1, 0},
                {0, 1, 0},
                {0, 1, 0}
        };
        board = new Board(matrix);


        Cell cell = board.getCell(1,1);
        int aliveNeighbourCount = 0;
        for (Cell neighbour : cell.neighbours) {
            aliveNeighbourCount += neighbour.getState();
        }

        Assertions.assertEquals(aliveNeighbourCount, 2);
    }

    @Test
    public void givenAllAliveNeighboursFindNeighboursReturnsEight(){
        int[][] matrix = new int[][]{
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}
        };
        board = new Board(matrix);

        Cell cell = board.getCell(1,1);
        int aliveNeighbourCount = 0;
        for (Cell neighbour : cell.neighbours) {
            aliveNeighbourCount += neighbour.getState();
        }

        Assertions.assertEquals(aliveNeighbourCount, 8);
    }
    @Test
    public void givenCellIndexesOneOneAliveBoardReturnsAliveCellState(){
        int[][] matrix = new int[][]{
                {0, 0, 0},
                {0, 1, 0},
                {0, 0, 0}
        };
        board = new Board(matrix);
        Cell cell = board.getCell(1,1);
        Assertions.assertEquals(cell.getState(), 1);
    }

    @Test
    public void givenBoardX5Y3ReturnsBoardSizeX5Y3(){
        int[][] matrix = new int[][]{
                {0, 0, 0, 0, 0},
                {0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0}
        };
        board = new Board(matrix);
        Assertions.assertEquals(board.sizeX, 5);
        Assertions.assertEquals(board.sizeY, 3);
    }
}
