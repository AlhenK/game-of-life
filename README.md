# Game of life

- TDD Introduction for Coderetreat event
- The project has "TDD kata" a series of exercises divided into "ping-pong" phases for pair-programming.
- The final result of Kata is an implementation of 4 rules, not the whole Conway's Game-of-life application.

## The objectives are
- to follow tasks in TestSuite.
- to start writing tests before code
- to split pair programming into ping-pong: one create a test, the other implement code

##### The root source of the project has an example of parametric tests with multiple test objects.

## Actions:
- [Check out the presentation](docs/presentation.md)
- Follow kata steps 01-08 in *com.training.gol.kata.ping[][].TestSuite*


