package com.training.gol.kata.ping03;

public class Board03 {
    private final int[][] matrix;

    public Board03(int[][] matrix) {
        this.matrix = matrix;
    }

    public int checkCell(int x, int y) {
        return matrix[x][y];
    }

    public void step() {

    }
}
