package com.training.gol.kata.ping03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestSuite {
    private static final int ALIVE = 1;
    private static final int X = 1;
    private static final int Y = 1;

    @Test
    public void givenMatrixCellX1Y1ReturnsStateAlive() {
        int[][] matrix = {
                {0, 0, 0},
                {0, 1, 0},
                {0, 0, 0}
        };
        Board03 board = new Board03(matrix);
        Assertions.assertEquals(ALIVE, board.checkCell(X, Y));
    }

    /*
       Task:
       Make this test green - implement step() which complies Underpopulation rule
       When the test is green do refactoring
       Add other test case for the rule
    */
    @Test
    public void givenUnderpopulatedCellX1Y1NextStateDead() {
        int[][] matrix = {
                {0, 0, 0},
                {0, 1, 0},
                {0, 0, 0}
        };
        Board03 board = new Board03(matrix);
        board.step();
        Assertions.assertEquals(0, board.checkCell(X, Y));
    }
}
