package com.training.gol.kata.pong02;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestSuite {
    private final static int ALIVE = 1;
    private final static int X = 1;
    private final static int Y = 1;

    /*
      Task:
      Do refactoring
      Create a test for Underpopulation rule
      add a method step() which calculates next state of the Cell
   */
    @Test
    public void givenMatrixCellX1Y1ReturnsStateAlive() {
        int [][] matrix = {
                {0,0,0},
                {0,1,0},
                {0,0,0}
        };
        Board02 board = new Board02(matrix);
        Assertions.assertEquals(ALIVE, board.checkCell(X, Y));
    }
}
