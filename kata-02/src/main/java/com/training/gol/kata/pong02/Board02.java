package com.training.gol.kata.pong02;

public class Board02 {
    private final int[][] matrix;

    public Board02(int[][] matrix) {
        this.matrix = matrix;
    }

    public int checkCell(int x, int y) {
        return matrix[x][y];
    }
}
