package com.training.gol.kata.ping05;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestSuite {
    private static final int ALIVE = 1;
    private static final int DEAD = 0;
    private static final int X = 1;
    private static final int Y = 1;

    @Test
    public void givenMatrixCellX1Y1ReturnsStateAlive() {
        int[][] matrix = {
                {0, 0, 0},
                {0, 1, 0},
                {0, 0, 0}
        };
        Board05 board = new Board05(matrix);
        Assertions.assertEquals(ALIVE, board.checkCell(X, Y));
    }

    @Test
    public void givenUnderpopulatedCellX1Y1NextStateDead() {
        int[][] matrix = {
                {0, 0, 0},
                {0, 1, 0},
                {0, 0, 0}
        };
        Board05 board = new Board05(matrix);
        board.step();
        Assertions.assertEquals(DEAD, board.checkCell(X, Y));
    }

    @Test
    public void givenOtherUnderpopulatedCellX1Y1NextStateDead() {
        int[][] matrix = {
                {0, 0, 0},
                {0, 1, 0},
                {0, 0, 1}
        };
        Board05 board = new Board05(matrix);
        board.step();
        Assertions.assertEquals(DEAD, board.checkCell(X, Y));
    }

    /*
      Task:
      Make these tests green - implement step()
      which complies Underpopulation and Survival rules
      When the test is green do refactoring
   */
    @Test
    public void givenOvercrowdingNeighboursCellX1Y1NextStateIsDead() {
        int[][] matrix = {
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}
        };
        Board05 board = new Board05(matrix);
        board.step();
        Assertions.assertEquals(DEAD, board.checkCell(X, Y));
    }

    @Test
    public void givenSurvivalNeighboursCellX1Y1NextStateIsAlive() {
        int[][] matrix = {
                {0, 0, 0},
                {0, 1, 1},
                {0, 0, 1}
        };
        Board05 board = new Board05(matrix);
        board.step();
        Assertions.assertEquals(ALIVE, board.checkCell(X, Y));
    }

}
