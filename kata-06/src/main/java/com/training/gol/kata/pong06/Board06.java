package com.training.gol.kata.pong06;

public class Board06 {
    private static final int X = 1;
    private static final int Y = 1;
    private static final int DEAD = 0;
    private final int[][] matrix;

    public Board06(int[][] matrix) {
        this.matrix = matrix;
    }

    public int checkCell(int x, int y) {
        return matrix[x][y];
    }

    public void step() {
        int aliveNeighbors = 0;
        for (int j = 0; j < matrix.length; j++) {
            for (int i = 0; i < matrix.length; i++) {
                if (!(i == X && j == Y)) {
                    aliveNeighbors += matrix[i][j];
                }
            }
        }
        if (aliveNeighbors < 2) {
            matrix[X][X] = DEAD;
        } else if (aliveNeighbors > 3) {
            matrix[X][X] = DEAD;
        }
    }
}
